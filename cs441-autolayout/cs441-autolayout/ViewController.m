//
//  ViewController.m
//  cs441-autolayout
//
//  Created by Patrick Madden on 2/9/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize counterLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [counterLabel setText:[NSString stringWithFormat:@"Counter: %d", [[Universe sharedInstance] counter]]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)performSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    NSLog(@"Performing segue with ID %@, so we can set things up.", identifier);
}

-(IBAction)unwindForSegue:(UIStoryboardSegue *)unwindSegue towardsViewController:(UIViewController *)subsequentVC
{
    NSLog(@"Backing out of the other view controller.");
    [counterLabel setText:[NSString stringWithFormat:@"Counter: %d", [[Universe sharedInstance] counter]]];

}

-(IBAction)incrementCounter:(id)sender
{
    int v = [[Universe sharedInstance] counter] + 1;
    [[Universe sharedInstance] setCounter:v];
    [counterLabel setText:[NSString stringWithFormat:@"Counter: %d", [[Universe sharedInstance] counter]]];
}
@end
