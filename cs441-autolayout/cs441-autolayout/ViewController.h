//
//  ViewController.h
//  cs441-autolayout
//
//  Created by Patrick Madden on 2/9/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Universe.h"

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet UILabel *counterLabel;
-(IBAction)incrementCounter:(id)sender;

@end

